import { LoginComponent } from './demo/view/login/login.component';
import { AuthGuardService } from './auth-guard.service';
import { SolicitarCartaoComponent } from './demo/view/solicitar-cartao/solicitar-cartao.component';
import { UsuarioComponent } from './demo/view/usuario/usuario.component';
import { UsuarioDetalheComponent } from './demo/view/usuario-detalhe/usuario-detalhe.component';
import { OfertaDetalheComponent } from './demo/view/oferta-detalhe/oferta-detalhe.component';
import { OfertaComponent } from './demo/view/oferta/oferta.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { DashboardDemoComponent } from './demo/view/dashboarddemo.component';
import { EmptyDemoComponent } from './demo/view/emptydemo.component';
import { CartaoComponent } from './demo/view/cartao/cartao.component';
import { CartaoDetalheComponent } from './demo/view/cartao-detalhe/cartao-detalhe.component';
import { NotificacaoComponent } from './demo/view/notificacao/notificacao.component';
import { NotificacaoDetalheComponent } from './demo/view/notificacao-detalhe/notificacao-detalhe.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    children: [
      { path: '', component: DashboardDemoComponent },
      { path: 'oferta', component: OfertaComponent },
      { path: 'oferta/novo', component: OfertaDetalheComponent },
      { path: 'oferta/:id', component: OfertaDetalheComponent },
      { path: 'usuario', component: UsuarioComponent },
      { path: 'usuario/novo', component: UsuarioDetalheComponent },
      { path: 'usuario/:id', component: UsuarioDetalheComponent },
      { path: 'solicitacaoCartao', component: SolicitarCartaoComponent },
      { path: 'cartao', component: CartaoComponent },
      { path: 'cartao/novo', component: CartaoDetalheComponent },
      { path: 'cartao/:id', component: CartaoDetalheComponent },
      { path: 'notificacao', component: NotificacaoComponent },
      { path: 'notificacao/novo', component: NotificacaoDetalheComponent },
      { path: 'notificacao/:id', component: NotificacaoDetalheComponent }
    ]
  },
  { path: 'login', component: LoginComponent }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);

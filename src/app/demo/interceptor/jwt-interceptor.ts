import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from '../domain/usuario';
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const usuario: Usuario = JSON.parse(localStorage.getItem('usuarioSessao'));
    const urlViaCep = 'https://viacep.com.br/';

    if (usuario != null && usuario.token) {
      if (urlViaCep.localeCompare(req.url) !== -1) {
        req = req.clone({
          setHeaders: {
            Authorization: `Bearer ${usuario.token}`
          }
        });
      }
    }
    return next.handle(req);
  }
}

import { Notificacao } from './../../domain/notificacao';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificacaoService } from '../../service/notificacao.service';
import { VariaveisGlobaisService } from '../../service/variaveis-globais.service';
import { BreadcrumbService } from 'src/app/breadcrumb.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-notificacao-detalhe',
  templateUrl: './notificacao-detalhe.component.html',
  styleUrls: ['./notificacao-detalhe.component.css']
})
export class NotificacaoDetalheComponent implements OnInit {
  notificacao: Notificacao;
  ptBR: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public notificacaoSrv: NotificacaoService,
    private locate: Location,
    private global: VariaveisGlobaisService,
    private breadcrumbService: BreadcrumbService
  ) {
    this.breadcrumbService.setItems([{ label: '' }]);
  }

  ngOnInit() {
    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
      ],
      monthNamesShort: [
        'Jan',
        'Fev',
        'Mar',
        'Abr',
        'Mai',
        'Jun',
        'Jul',
        'Ago',
        'Set',
        'Out',
        'Nov',
        'Dez'
      ],
      today: 'Hoje',
      clear: 'Limpar'
    };

    this.notificacao = new Notificacao();
    if (this.router.url !== '/notificacao/novo') {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Notificacao/editar', routerLink: ['/notificacao'] }
      ]);
      const id = this.route.snapshot.paramMap.get('id');
      this.notificacaoSrv.editar(id).subscribe(
        obj => {
          this.notificacao = obj;
        },
        error => {
          this.global.tratarError(error);
        }
      );
    } else {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Notificacao/novo', routerLink: ['/notificacao'] }
      ]);
    }
  }

  salvar() {
    this.notificacaoSrv.salvar(this.notificacao).subscribe(
      result => {
        this.notificacao = result;
        this.global.mostraMensagem(
          this.global.info,
          'Sucesso!',
          this.notificacao.id == null
            ? 'Notificacao salva com Sucesso'
            : 'Notificacao alterada com Sucesso'
        );
        this.cancelar();
      },
      error => {
        this.global.tratarError(error);
      }
    );
  }

  cancelar() {
    this.locate.back();
  }
}

import { UsuarioService } from './../../service/usuario.service';
import { Usuario } from './../../domain/usuario';
import { CartaoService } from './../../service/cartao.service';
import { Cartao } from './../../domain/cartao';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { VariaveisGlobaisService } from '../../service/variaveis-globais.service';
import { BreadcrumbService } from 'src/app/breadcrumb.service';

@Component({
  selector: 'app-cartao-detalhe',
  templateUrl: './cartao-detalhe.component.html',
  styleUrls: ['./cartao-detalhe.component.css']
})
export class CartaoDetalheComponent implements OnInit {
  cartao: Cartao;
  usuarios: Usuario[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public cartaoSrv: CartaoService,
    private usuarioSrv: UsuarioService,
    private locate: Location,
    private global: VariaveisGlobaisService,
    private breadcrumbService: BreadcrumbService
  ) {
    this.breadcrumbService.setItems([{ label: '' }]);
    this.usuarios = [];
    this.lsitaUsuarios();
  }

  ngOnInit() {
    this.cartao = new Cartao();
    if (this.router.url !== '/cartao/novo') {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Cartao/editar', routerLink: ['/cartao'] }
      ]);
      const id = this.route.snapshot.paramMap.get('id');
      this.cartaoSrv.editar(id).subscribe(
        obj => {
          this.cartao = obj;
        },
        error => {
          this.global.tratarError(error);
        }
      );
    } else {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Cartão/novo', routerLink: ['/cartao'] }
      ]);
    }
  }

  salvarCartao() {
    if (this.cartao.id) {
      if (this.cartao.valorFatura > this.cartao.limite) {
        this.global.mostraMensagem(this.global.warn, 'Atencao!', 'Limite Indisponivel!');
        return;
      }
    }

    if (this.cartao.id == null) {
      this.cartao.status = true;
    }

    this.cartao.limiteDisponivel = this.cartao.limite - this.cartao.valorFatura;
    this.cartaoSrv.salvarCartao(this.cartao).subscribe(
      result => {
        this.cartao = result;
        this.global.mostraMensagem(
          this.global.info,
          'Sucesso!',
          this.cartao.id == null ? 'Oferta salva com Sucesso' : 'Oferta alterada com Sucesso'
        );
        this.cancelar();
      },
      error => {
        this.global.tratarError(error);
      }
    );
  }

  cancelar() {
    this.locate.back();
  }

  lsitaUsuarios() {
    this.usuarioSrv.buscar().subscribe(result => {
      this.usuarios = result['content'];
    });
  }
}

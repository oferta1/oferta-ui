import { VariaveisGlobaisService } from './../../service/variaveis-globais.service';
import { Oferta } from './../../domain/oferta';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OfertaService } from '../../service/oferta.service';
import { BreadcrumbService } from 'src/app/breadcrumb.service';
import { Location } from '@angular/common';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-oferta-detalhe',
  templateUrl: './oferta-detalhe.component.html',
  styleUrls: ['./oferta-detalhe.component.css']
})
export class OfertaDetalheComponent implements OnInit {
  periodosO: SelectItem[];
  periodo: string;
  oferta: Oferta;
  urlFoto: any;
  urlUpload: string;
  uploadedFiles: any[] = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public ofertaSrv: OfertaService,
    private locate: Location,
    private global: VariaveisGlobaisService,
    private breadcrumbService: BreadcrumbService
  ) {
    this.breadcrumbService.setItems([{ label: '' }]);
  }

  ngOnInit() {
    this.periodosO = [
      { label: 'Oferta da Semana', value: 'PERIODO_SEMANA' },
      { label: 'Oferta do Dia', value: 'PERIODO_DIA' },
      { label: 'Oferta do Mês', value: 'PERIODO_MES' }
    ];
    this.oferta = new Oferta();
    if (this.router.url !== '/oferta/novo') {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Oferta/editar', routerLink: ['/oferta'] }
      ]);
      const id = this.route.snapshot.paramMap.get('id');
      this.ofertaSrv.editar(id).subscribe(
        obj => {
          this.oferta = obj;
        },
        error => {
          this.global.tratarError(error);
        }
      );
    } else {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Oferta/novo', routerLink: ['/oferta'] }
      ]);
    }
  }

  salvarOferta() {
    this.ofertaSrv.salvarOferta(this.oferta).subscribe(
      result => {
        this.oferta = result;
        this.global.mostraMensagem(
          this.global.info,
          'Sucesso!',
          this.oferta.id == null ? 'Oferta salva com Sucesso' : 'Oferta alterada com Sucesso'
        );
        this.cancelar();
      },
      error => {
        this.global.tratarError(error);
      }
    );
  }

  cancelar() {
    this.locate.back();
  }

  selecionarTipoOferta(event) {
    this.ofertaSrv.oferta.periodoOferta = event.value.toString();
  }

  myUploader(event) {
    const arquivo = <File>event.files[0];
    this.uploadedFiles.push(event.files);
    const reader = new FileReader();
    reader.onloadend = e => {
      this.oferta.imagem = reader.result.toString();
    };
    reader.readAsDataURL(arquivo);
  }
}

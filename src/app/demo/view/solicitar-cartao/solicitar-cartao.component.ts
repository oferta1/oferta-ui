import { Usuario } from './../../domain/usuario';
import { SolicitarCartao } from './../../domain/solicitar-cartao';
import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/breadcrumb.service';
import { SolicitarCartaoService } from '../../service/solicitar-cartao.service';
import { ConfirmationService } from 'primeng/api';
import { VariaveisGlobaisService } from '../../service/variaveis-globais.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-solicitar-cartao',
  templateUrl: './solicitar-cartao.component.html',
  styleUrls: ['./solicitar-cartao.component.css'],
  styles: [
    `
      /* Column Priorities */
      @media only all {
        th.ui-p-6,
        td.ui-p-6,
        th.ui-p-5,
        td.ui-p-5,
        th.ui-p-4,
        td.ui-p-4,
        th.ui-p-3,
        td.ui-p-3,
        th.ui-p-2,
        td.ui-p-2,
        th.ui-p-1,
        td.ui-p-1 {
          display: none;
        }
      }

      /* Show priority 1 at 320px (20em x 16px) */
      @media screen and (min-width: 20em) {
        th.ui-p-1,
        td.ui-p-1 {
          display: table-cell;
        }
      }

      /* Show priority 2 at 480px (30em x 16px) */
      @media screen and (min-width: 30em) {
        th.ui-p-2,
        td.ui-p-2 {
          display: table-cell;
        }
      }

      /* Show priority 3 at 640px (40em x 16px) */
      @media screen and (min-width: 40em) {
        th.ui-p-3,
        td.ui-p-3 {
          display: table-cell;
        }
      }

      /* Show priority 4 at 800px (50em x 16px) */
      @media screen and (min-width: 50em) {
        th.ui-p-4,
        td.ui-p-4 {
          display: table-cell;
        }
      }

      /* Show priority 5 at 960px (60em x 16px) */
      @media screen and (min-width: 60em) {
        th.ui-p-5,
        td.ui-p-5 {
          display: table-cell;
        }
      }

      /* Show priority 6 at 1,120px (70em x 16px) */
      @media screen and (min-width: 70em) {
        th.ui-p-6,
        td.ui-p-6 {
          display: table-cell;
        }
      }
    `
  ]
})
export class SolicitarCartaoComponent implements OnInit {
  solCartao: SolicitarCartao;
  solCartaoSelecionado: SolicitarCartao;
  solCartoes: SolicitarCartao[] = [];
  filtro: SolicitarCartao;
  fileUrl;
  display: boolean = false;
  constructor(
    private breadcrumbService: BreadcrumbService,
    public solCartaoSrv: SolicitarCartaoService,
    private confirmationService: ConfirmationService,
    private sanitizer: DomSanitizer,
    private global: VariaveisGlobaisService
  ) {
    this.breadcrumbService.setItems([
      { label: 'Cadastro' },
      { label: 'Solicitação de Cartão', routerLink: ['/solicitacaoCartao'] }
    ]);
  }

  ngOnInit() {
    this.solCartao = new SolicitarCartao();
    this.solCartaoSelecionado = new SolicitarCartao();
    this.inicializarFiltro();
    this.solCartoes = [];
    this.consultar();
  }

  consultar() {
    this.solCartaoSrv.buscar(this.filtro).subscribe(
      result => {
        this.solCartoes = result['content'];
      },
      error => {
        this.global.tratarError(error);
      }
    );
  }

  inicializarFiltro() {
    this.filtro = new SolicitarCartao();
    this.filtro.nome = '';
  }

  autorizarCartao(event, s: SolicitarCartao) {
    this.filtro.status = event.checked;
    this.solCartaoSrv.autorizaCartao(this.filtro, s).subscribe(
      () => {
        this.global.mostraMensagem(
          this.global.info,
          'Atenção',
          this.filtro.status ? 'Cartão Autorizado!' : 'Cartão Regeitado!'
        );
      },
      error => {
        this.global.tratarError(error);
      }
    );
  }

  baixarAnexo(s: SolicitarCartao) {
    this.solCartaoSrv.donwloadArquivo(s).subscribe(result => {
      this.global.mostraMensagem(this.global.info, 'Sucesso', 'Arquivo Baixado em : ' + result);
    });
  }

  visualizarEndereco(s: SolicitarCartao) {
    this.solCartaoSelecionado = s;
    this.display = true;
  }
}

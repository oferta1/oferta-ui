import { VariaveisGlobaisService } from './../../service/variaveis-globais.service';
import { UsuarioService } from './../../service/usuario.service';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../domain/usuario';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

interface JWTPayload {
  user_id: number;
  username: string;
  email: string;
  exp: number;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usuario: Usuario = new Usuario();
  formUsuario: FormGroup;
  submitted = false;

  constructor(
    public usuarioSrv: UsuarioService,
    public router: Router,
    private formBuilder: FormBuilder,
    private global: VariaveisGlobaisService
  ) {
    this.usuarioSrv.usuarioLogado = new Usuario();
  }

  ngOnInit() {
    this.usuario = new Usuario();
    this.createForm(this.usuario);
  }

  logar() {
    if (this.formUsuario.invalid) {
      return;
    }
    this.usuarioSrv.logar(this.formUsuario.value).subscribe(
      result => {
        this.usuarioSrv.usuarioLogado = result as Usuario;
        if (this.usuarioSrv.usuarioLogado.auths.indexOf('ROLE_APP') !== -1) {
          this.global.mostraMensagem(
            this.global.warn,
            'Permissão!',
            'Usaurio sem permissão para area Administrativa!'
          );
        } else {
          this.setSession(this.usuarioSrv.usuarioLogado);
          this.router.navigate(['/']);
        }
      },
      error => {
        this.global.tratarError(error);
      }
    );
  }

  private setSession(authResult: Usuario) {
    const token = authResult.token;
    const payload = <JWTPayload>jwt_decode(token);

    localStorage.setItem('usuarioSessao', JSON.stringify(authResult));
    localStorage.setItem('tempoExpira', JSON.stringify(payload.exp.valueOf()));
  }

  createForm(usuario: Usuario) {
    this.formUsuario = this.formBuilder.group({
      cpf: [usuario.cpf, [Validators.required]],
      senha: [usuario.senha, [Validators.required, Validators.minLength(1)]]
    });
  }
}

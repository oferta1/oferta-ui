import { Permissao } from './../../domain/permissao';
import { PermisssaoService } from './../../service/permisssao.service';
import { UsuarioService } from './../../service/usuario.service';
import { Usuario } from './../../domain/usuario';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VariaveisGlobaisService } from '../../service/variaveis-globais.service';
import { BreadcrumbService } from 'src/app/breadcrumb.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-usuario-detalhe',
  templateUrl: './usuario-detalhe.component.html',
  styleUrls: ['./usuario-detalhe.component.css']
})
export class UsuarioDetalheComponent implements OnInit {
  periodo: string;
  usuario: Usuario;
  permissaoSelecionada: Permissao = new Permissao();
  permissoes: Permissao[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public usuarioSrv: UsuarioService,
    private locate: Location,
    public global: VariaveisGlobaisService,
    private breadcrumbService: BreadcrumbService,
    private permissaoSrv: PermisssaoService
  ) {
    this.breadcrumbService.setItems([{ label: '' }]);
  }

  ngOnInit() {
    this.carregarPermissao();
    this.usuario = new Usuario();
    this.usuario.permissoes = [];
    if (this.router.url !== '/usuario/novo') {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Usuario/editar', routerLink: ['/usuario'] }
      ]);
      const id = this.route.snapshot.paramMap.get('id');
      this.usuarioSrv.editar(id).subscribe(
        obj => {
          this.usuario = obj;
        },
        error => {
          this.global.tratarError(error);
        }
      );
    } else {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Usuario/novo', routerLink: ['/usuario'] }
      ]);
    }
  }

  salvarUsuario() {
    this.usuario.ativo = true;
    this.usuarioSrv.salvarUsuario(this.usuario).subscribe(
      result => {
        this.usuario = result;
        this.global.mostraMensagem(
          this.global.info,
          'Sucesso!',
          this.usuario.id == null ? 'Oferta salva com Sucesso' : 'Oferta alterada com Sucesso'
        );
        this.cancelar();
      },
      error => {
        this.global.tratarError(error);
      }
    );
  }

  cancelar() {
    this.locate.back();
  }

  carregarPermissao() {
    this.permissoes = [];
    this.permissaoSrv.findAll().subscribe(result => {
      this.permissoes = result['content'];
    });
  }

  inserirPermissao() {
    for (let i = 0; i < this.usuario.permissoes.length; i++) {
      if (this.permissaoSelecionada.id === this.usuario.permissoes[i].id) {
        this.global.mostraMensagem(this.global.warn, 'Atenção', 'Permissão já inserida!');
        return;
      }
    }
    this.usuario.permissoes.push(this.permissaoSelecionada);
  }

  excluirPermissao(permissao: Permissao) {
    const remov = this.usuario.permissoes.indexOf(permissao);
    this.usuario.permissoes.splice(remov, 1);
  }

  validarCpf(cpf: string) {
    if (cpf.length === 14) {
      cpf = cpf.replace(/\D/g, '');
      // if (cpf == null) {
      //   return false;
      // }
      // if (cpf.length !== 11) {
      //   return false;
      // }
      if (
        cpf === '00000000000' ||
        cpf === '11111111111' ||
        cpf === '22222222222' ||
        cpf === '33333333333' ||
        cpf === '44444444444' ||
        cpf === '55555555555' ||
        cpf === '66666666666' ||
        cpf === '77777777777' ||
        cpf === '88888888888' ||
        cpf === '99999999999'
      ) {
        this.global.mostraMensagem(
          this.global.warn,
          'Atenção!',
          'Opps... cpf não pode conter 00000 e/ou 999999!'
        );
        cpf = '';
        return;
      }
      let numero: number = 0;
      let caracter: string = '';
      let numeros: string = '0123456789';
      let j: number = 10;
      let somatorio: number = 0;
      let resto: number = 0;
      let digito1: number = 0;
      let digito2: number = 0;
      let cpfAux: string = '';
      cpfAux = cpf.substring(0, 9);
      for (let i: number = 0; i < 9; i++) {
        caracter = cpfAux.charAt(i);
        if (numeros.search(caracter) === -1) {
          cpf = '';
          this.global.mostraMensagem(this.global.warn, 'Atenção!', 'Opps... erro desconhecido');
          return;
        }
        numero = Number(caracter);
        somatorio = somatorio + numero * j;
        j--;
      }
      resto = somatorio % 11;
      digito1 = 11 - resto;
      if (digito1 > 9) {
        digito1 = 0;
      }
      j = 11;
      somatorio = 0;
      cpfAux = cpfAux + digito1;
      for (let i: number = 0; i < 10; i++) {
        caracter = cpfAux.charAt(i);
        numero = Number(caracter);
        somatorio = somatorio + numero * j;
        j--;
      }
      resto = somatorio % 11;
      digito2 = 11 - resto;
      if (digito2 > 9) {
        digito2 = 0;
      }
      cpfAux = cpfAux + digito2;
      if (cpf !== cpfAux) {
        cpf = '';
        this.global.mostraMensagem(this.global.warn, 'Atenção!', 'Opps... Cpf invalido');
        return;
      } else {
        this.global.mostraMensagem(this.global.success, 'Sucesso!', 'Cpf Valido!');
        return;
      }
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../domain/usuario';
import { BreadcrumbService } from 'src/app/breadcrumb.service';
import { UsuarioService } from '../../service/usuario.service';
import { ConfirmationService } from 'primeng/api';
import { VariaveisGlobaisService } from '../../service/variaveis-globais.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
  styles: [
    `
      /* Column Priorities */
      @media only all {
        th.ui-p-6,
        td.ui-p-6,
        th.ui-p-5,
        td.ui-p-5,
        th.ui-p-4,
        td.ui-p-4,
        th.ui-p-3,
        td.ui-p-3,
        th.ui-p-2,
        td.ui-p-2,
        th.ui-p-1,
        td.ui-p-1 {
          display: none;
        }
      }

      /* Show priority 1 at 320px (20em x 16px) */
      @media screen and (min-width: 20em) {
        th.ui-p-1,
        td.ui-p-1 {
          display: table-cell;
        }
      }

      /* Show priority 2 at 480px (30em x 16px) */
      @media screen and (min-width: 30em) {
        th.ui-p-2,
        td.ui-p-2 {
          display: table-cell;
        }
      }

      /* Show priority 3 at 640px (40em x 16px) */
      @media screen and (min-width: 40em) {
        th.ui-p-3,
        td.ui-p-3 {
          display: table-cell;
        }
      }

      /* Show priority 4 at 800px (50em x 16px) */
      @media screen and (min-width: 50em) {
        th.ui-p-4,
        td.ui-p-4 {
          display: table-cell;
        }
      }

      /* Show priority 5 at 960px (60em x 16px) */
      @media screen and (min-width: 60em) {
        th.ui-p-5,
        td.ui-p-5 {
          display: table-cell;
        }
      }

      /* Show priority 6 at 1,120px (70em x 16px) */
      @media screen and (min-width: 70em) {
        th.ui-p-6,
        td.ui-p-6 {
          display: table-cell;
        }
      }
    `
  ]
})
export class UsuarioComponent implements OnInit {
  usuario: Usuario;
  usuarios: Usuario[];
  filtro: Usuario;
  constructor(
    private breadcrumbService: BreadcrumbService,
    public usuarioSrv: UsuarioService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobaisService
  ) {
    this.breadcrumbService.setItems([
      { label: 'Cadastro' },
      { label: 'Usuario', routerLink: ['/usuario'] }
    ]);
  }

  ngOnInit() {
    this.usuario = new Usuario();
    this.inicializarFiltro();
    this.usuarios = [];
    this.consultar();
  }
  consultar() {
    this.usuarioSrv.buscar(this.filtro).subscribe(
      result => {
        this.usuarios = result['content'];
      },
      (responseError: HttpErrorResponse) => {
        this.global.tratarError(responseError);
      }
    );
  }
  editarUsuario(u: Usuario) {
    return u.id;
  }

  excluirUsuario(obj: Usuario) {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.usuarioSrv.deletar(obj).subscribe(
          () => {
            this.global.mostraMensagem(this.global.info, 'Confirmar', 'cargo Excluido!');
            this.consultar();
          },
          error => {
            this.global.tratarError(error);
          }
        );
      },
      reject: () => {}
    });
  }

  inicializarFiltro() {
    this.filtro = new Usuario();
    this.filtro.nome = '';
  }
}

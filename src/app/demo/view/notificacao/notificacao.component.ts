import { Notificacao } from './../../domain/notificacao';
import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/breadcrumb.service';
import { NotificacaoService } from '../../service/notificacao.service';
import { ConfirmationService } from 'primeng/api';
import { VariaveisGlobaisService } from '../../service/variaveis-globais.service';

@Component({
  selector: 'app-notificacao',
  templateUrl: './notificacao.component.html',
  styleUrls: ['./notificacao.component.css'],
  styles: [
    `
      /* Column Priorities */
      @media only all {
        th.ui-p-6,
        td.ui-p-6,
        th.ui-p-5,
        td.ui-p-5,
        th.ui-p-4,
        td.ui-p-4,
        th.ui-p-3,
        td.ui-p-3,
        th.ui-p-2,
        td.ui-p-2,
        th.ui-p-1,
        td.ui-p-1 {
          display: none;
        }
      }

      /* Show priority 1 at 320px (20em x 16px) */
      @media screen and (min-width: 20em) {
        th.ui-p-1,
        td.ui-p-1 {
          display: table-cell;
        }
      }

      /* Show priority 2 at 480px (30em x 16px) */
      @media screen and (min-width: 30em) {
        th.ui-p-2,
        td.ui-p-2 {
          display: table-cell;
        }
      }

      /* Show priority 3 at 640px (40em x 16px) */
      @media screen and (min-width: 40em) {
        th.ui-p-3,
        td.ui-p-3 {
          display: table-cell;
        }
      }

      /* Show priority 4 at 800px (50em x 16px) */
      @media screen and (min-width: 50em) {
        th.ui-p-4,
        td.ui-p-4 {
          display: table-cell;
        }
      }

      /* Show priority 5 at 960px (60em x 16px) */
      @media screen and (min-width: 60em) {
        th.ui-p-5,
        td.ui-p-5 {
          display: table-cell;
        }
      }

      /* Show priority 6 at 1,120px (70em x 16px) */
      @media screen and (min-width: 70em) {
        th.ui-p-6,
        td.ui-p-6 {
          display: table-cell;
        }
      }
    `
  ]
})
export class NotificacaoComponent implements OnInit {
  notificacao: Notificacao;
  notificacoes: Notificacao[] = [];
  filtro: Notificacao;
  constructor(
    private breadcrumbService: BreadcrumbService,
    public notificacaoSrv: NotificacaoService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobaisService
  ) {
    this.breadcrumbService.setItems([
      { label: 'Cadastro' },
      { label: 'Notificação', routerLink: ['/notificacao'] }
    ]);
  }

  ngOnInit() {
    this.notificacao = new Notificacao();
    this.inicializarFiltro();
    this.notificacoes = [];
    this.consultar();
  }

  consultar() {
    this.notificacaoSrv.buscar(this.filtro).subscribe(
      result => {
        this.notificacoes = result['content'];
      },
      error => {
        this.global.tratarError(error);
      }
    );
  }
  editar(of: Notificacao) {
    return of.id;
  }

  excluir(obj: Notificacao) {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.notificacaoSrv.deletar(obj).subscribe(
          () => {
            this.global.mostraMensagem(this.global.info, 'Confirmar', 'Notificação Excluido!');
            this.consultar();
          },
          error => {
            this.global.tratarError(error);
          }
        );
      },
      reject: () => {}
    });
  }

  inicializarFiltro() {
    this.filtro = new Notificacao();
    this.filtro.nome = '';
  }
}

export class Notificacao {
  id: number;
  nome: string;
  descricao: string;
  dataNotificacao: Date;
}

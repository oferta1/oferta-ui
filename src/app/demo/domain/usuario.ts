import { Permissao } from './permissao';
export class Usuario {
  id: number;
  ativo: boolean;
  email: string;
  nome: string;
  senha: string;
  cpf: string;
  token: string;
  auths: string[] = [];
  permissoes: Permissao[] = [];
}

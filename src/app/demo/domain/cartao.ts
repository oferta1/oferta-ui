import { SolicitarCartao } from './solicitar-cartao';
import { Usuario } from './usuario';
export class Cartao {
  id: number;
  limite: number;
  limiteDisponivel: number;
  nomeCliente: string;
  numeroCartao: string;
  senhaCartao: string;
  status: boolean;
  valorFatura: number;
  usuarioCartao: Usuario;
  bancoCartao: string;
  expiraEm: string;
  logoCartao: string;
}

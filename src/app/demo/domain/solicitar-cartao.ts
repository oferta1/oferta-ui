import { Usuario } from './usuario';
export class SolicitarCartao {
  id: number;
  cpf: string;
  dataNascimento: Date;
  documento: string;
  email: string;
  nome: string;
  profissao: string;
  rendaMensal: string;
  status: boolean;
  telefone: string;
  bairro: string;
  cep: string;
  numero: string;
  complemento: string;
  localidade: string;
  logradouro: string;
  uf: string;
  usuario: Usuario;
}

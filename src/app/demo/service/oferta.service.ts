import { Oferta } from './../domain/oferta';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OfertaService extends CrudService<Oferta, number> {
  oferta: Oferta;
  constructor(public http: HttpClient) {
    super(http, environment.urlApi + '/oferta');
  }

  salvarOferta(oferta: Oferta): Observable<Oferta> {
    if (oferta.id === undefined) {
      return this.save(oferta, environment.headers);
    } else {
      return this.update(oferta.id, oferta, environment.headers);
    }
  }
  buscar(f: Oferta): Observable<Oferta[]> {
    let params = new HttpParams();
    if (f !== undefined) {
      params = params.append('nomeProduto', f.nomeProduto);
    }
    return this.findAll(environment.headers, params);
  }
  editar(id: string): Observable<Oferta> {
    return this.findOne(parseInt(id, 0), environment.headers);
  }

  deletar(obj: Oferta): Observable<{}> {
    return this.delete(obj.id, environment.headers);
  }
}

import { HttpClient } from '@angular/common/http';
import { Permissao } from './../domain/permissao';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PermisssaoService extends CrudService<Permissao, number> {
  constructor(public http: HttpClient) {
    super(http, environment.urlApi + '/permissao');
  }
}

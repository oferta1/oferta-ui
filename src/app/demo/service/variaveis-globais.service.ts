import { UsuarioService } from './usuario.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Message } from 'primeng/api';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VariaveisGlobaisService {
  tituloJanela = '';
  enderecoUrl;
  msgs: Message[] = [];
  success = 'success';
  info = 'info';
  warn = 'warn';
  error = 'error';
  botaoDesabilitado: Boolean = true;
  loading: Boolean = false;

  constructor(private router: Router, private usuarioSrv: UsuarioService) {}

  /**
   * @param tipo  = `success`, `info`, `warn`, `error`
   * @param titulo = `cabeçalho da mensagem`
   * @param mensagem = `corpo da mensagem`
   */
  mostraMensagem(tipo: string, titulo: string, mensagem: string) {
    this.msgs = [{ severity: tipo, summary: titulo, detail: mensagem }];
  }

  tratarError(error: HttpErrorResponse): string {
    if (error.status === 404) {
      this.mostraMensagem(this.warn, 'Atenção!', 'Opps... ' + error.statusText);
      this.router.navigate(['/']);
      this.tituloJanela = '';
    }

    if (error.status === 401) {
      if (error.error.trace.indexOf('java.sql.SQLIntegrityConstraintViolationException') !== -1) {
        this.mostraMensagem(this.warn, 'Atenção!', 'Cpf já cadastrado');
        return;
      }
      this.mostraMensagem(this.warn, 'Atenção!', 'Opps... Usuario e/ou Senha invalidos');
      this.usuarioSrv.logOut();
    }

    if (error.status === 400) {
      this.mostraMensagem(this.warn, 'Atenção!', 'Opps... ' + error.statusText);
      this.router.navigate(['/']);
      this.tituloJanela = '';
    }

    if (error.status === 500) {
      this.mostraMensagem(this.warn, 'Atenção!', 'Opps... ' + error.statusText);
      this.usuarioSrv.logOut();
    }

    if (error.error) {
      return error.status + ' - ' + error.error.message;
    } else {
      return 'Error desconhecido';
    }
  }
}

import { Notificacao } from './../domain/notificacao';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificacaoService extends CrudService<Notificacao, number> {
  constructor(public http: HttpClient) {
    super(http, environment.urlApi + '/notificacao');
  }

  salvar(notificacao: Notificacao): Observable<Notificacao> {
    if (notificacao.id === undefined) {
      return this.save(notificacao, environment.headers);
    } else {
      return this.update(notificacao.id, notificacao, environment.headers);
    }
  }
  buscar(n: Notificacao): Observable<Notificacao[]> {
    let params = new HttpParams();
    if (n !== undefined) {
      params = params.append('nome', n.nome);
    }
    return this.findAll(environment.headers, params);
  }
  editar(id: string): Observable<Notificacao> {
    return this.findOne(parseInt(id, 0), environment.headers);
  }

  deletar(obj: Notificacao): Observable<{}> {
    return this.delete(obj.id, environment.headers);
  }
}

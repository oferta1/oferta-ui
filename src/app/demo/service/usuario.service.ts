import { environment } from './../../../environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Usuario } from './../domain/usuario';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends CrudService<Usuario, number> {
  usuarioLogado: Usuario = new Usuario();
  constructor(public http: HttpClient, private router: Router) {
    super(http, environment.urlApi + '/usuario');
  }

  salvarUsuario(usuario: Usuario): Observable<Usuario> {
    if (usuario.id === undefined) {
      return this.save(usuario, environment.headers);
    } else {
      return this.update(usuario.id, usuario, environment.headers);
    }
  }

  getUsuarioLogado() {
    if (localStorage.getItem('usuarioSessao') != null) {
      return true;
    }
    return false;
  }

  buscar(f?: Usuario): Observable<Usuario[]> {
    let params = new HttpParams();
    if (f !== undefined) {
      params = params.append('nome', f.nome);
    }
    params = params.append('ativo', String(true));
    return this.findAll(environment.headers, params);
  }
  editar(id: string): Observable<Usuario> {
    return this.findOne(parseInt(id, 0), environment.headers);
  }

  deletar(obj: Usuario): Observable<{}> {
    return this.delete(obj.id, environment.headers);
  }

  logar(usuario: Usuario): Observable<Usuario> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json'
    });
    return this.http.post<Usuario>(environment.urlApi + '/login', usuario, { headers }); //this.findAll(environment.headers, params);
  }

  logOut() {
    localStorage.removeItem('usuarioSessao');
    localStorage.removeItem('tempoExpira');
    this.router.navigate(['/login']);
  }

  /**
   * let params = new HttpParams();
    params = params.append('cpf', usuario.cpf);
    params = params.append('senha', usuario.senha);
    params = params.append('ativo', String(true));
   */
}

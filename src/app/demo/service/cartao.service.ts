import { environment } from './../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cartao } from './../domain/cartao';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartaoService extends CrudService<Cartao, number> {
  columns: any[];
  constructor(public http: HttpClient) {
    super(http, environment.urlApi + '/cartao');
  }
  salvarCartao(cartao: Cartao): Observable<Cartao> {
    if (cartao.id === undefined) {
      return this.save(cartao, environment.headers);
    } else {
      return this.update(cartao.id, cartao, environment.headers);
    }
  }
  buscar(cartao: Cartao): Observable<Cartao[]> {
    let params = new HttpParams();
    if (cartao !== undefined) {
      params = params.append('nomeCliente', cartao.nomeCliente);
    }
    return this.findAll(environment.headers, params);
  }
  editar(id: string): Observable<Cartao> {
    return this.findOne(parseInt(id, 0), environment.headers);
  }

  deletar(cartao: Cartao): Observable<{}> {
    return this.delete(cartao.id, environment.headers);
  }

  relatorioPdf(cartoes: Cartao[]) {
    this.columns.push(
      'Nome do Cliente',
      'Cartão',
      'V.Fatura',
      'Limite',
      'Limite Disponivel',
      'Expira em?'
    );
    const cartao = new Cartao();
    const dados = [{}];
    for (const c in cartoes) {
      if (cartoes.hasOwnProperty(c)) {
        cartao.nomeCliente = cartoes[c].nomeCliente;
        cartao.numeroCartao = cartoes[c].numeroCartao;
        cartao.valorFatura = cartoes[c].valorFatura;
        cartao.limite = cartoes[c].limite;
        cartao.limiteDisponivel = cartoes[c].limiteDisponivel;
        cartao.expiraEm = cartoes[c].expiraEm;

        dados.push(cartao);
      }
    }
  }
}

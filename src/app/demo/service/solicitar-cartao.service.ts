import { environment } from './../../../environments/environment';
import { SolicitarCartao } from './../domain/solicitar-cartao';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SolicitarCartaoService extends CrudService<SolicitarCartao, number> {
  constructor(public http: HttpClient) {
    super(http, environment.urlApi + '/solicitacaoCartao');
  }

  buscar(c: SolicitarCartao): Observable<SolicitarCartao[]> {
    let params = new HttpParams();
    if (c !== undefined) {
      params = params.append('nome', c.nome);
    }
    return this.findAll(environment.headers, params);
  }

  autorizaCartao(
    filtro: SolicitarCartao,
    solicitacao: SolicitarCartao
  ): Observable<SolicitarCartao> {
    let params = new HttpParams();
    params = params.append('status', String(filtro.status));
    return this.update(solicitacao.id, solicitacao, environment.headers, params);
  }

  donwloadArquivo(c: SolicitarCartao): Observable<{}> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json'
    });
    return this.http.post<SolicitarCartao>(environment.urlApi + '/solicitacaoCartao/baixar', c, {
      headers
    });
  }
}
